# ATC CLI (ATC Tools Client CLI)

A client for ATC tools, services, and pipelines.

Have you ever thought to yourself "Wow, it sure is hard to work with ATC's pipelines. I wish it was easier..."?

Well think no more! ATC CLI is here to save the day and do your thinking for you. Let your tired brian go smooth for a
change.

## Installing

TODO

## Running

### Pre-requisites

#### Configuration files

The ATC CLI configuration consists of two main config files, a user-config and a remote_config. Both of these configs
are required for the ATC CLI to function.

Based on the declared profile when calling `atc --profile <profile_name>`, the CLI will gather the user config and
remote config, and merge it into a single in-memory config during the CLI's runtime.

##### User Config

Holds user-specific configuration, such as credentials, tokens, usernames, keys and other secrets
that the ATC CLI will use to interact with ATC services.

The user config must be created in `~/.config/atc/user_conf.toml`, alternatively the user can create the user
config in a filepath of their choosing, and set `ATC_USER_CONFIG` to point to it.

The structure of the user_config must be as follows:

```toml
[autosd] # Declares the profile name, this must match the profile name found in the remote_config_url
remote_config_url = "https://gitlab.com/hstefans/atc-cli-test-conf/-/raw/main/test_conf.toml?ref_type=heads"
my_token = "some-super-secret-value-here" # an example of a user-owned secret

[example] # The user config file can declare multiple profiles
remote_config_url = "https://gitlab.com/hstefans/atc-cli-test-conf/-/raw/main/another_test_conf.toml?ref_type=heads"
```

##### Remote Config

A hosted configuration file used to keep service endpoints and other semi-variable data up-to-date without requiring the
user to update it manually.

The ATC CLI will automatically create the `cache_config.toml` for you when you declare a valid `remote_config_url` in
your user config.
The remote config will be updated automatically every day, there's no need for you to do anything with this file. Unless
you want to force a config update manually, in which case you can simply delete the cache.

The top level profile declaration `[autosd]` must match that of the remote config

### Usage

#### Get

##### Status

## Extending

ATC CLI supports custom extensions. Simply:

1. Do the thing
2. Build a distributable
3. Install it in the same environment as ATC CLI
4. Profit

## Contributing

This project uses [PDM][_pdm] to manage project metadata, builds, dependencies, and virtual environment. For more on
installing and working with PDM visit their [docs][_pdm].


[_pdm]: https://pdm.fming.dev/latest/
