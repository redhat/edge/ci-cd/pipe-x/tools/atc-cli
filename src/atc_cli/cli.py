"""
cli is the main class of the ATC CLI.
It enables the integration and abstraction of ATC services and tools into a single interface, through a human friendly
verb-based command structure. allowing users to perform CRUD actions in a natural way, I.E `atc get nightly x86` will
(in the future) return the latest ATC supported image build from the defined profile.
"""

import click

from status.commands import get_status
from utils.config import ATC


@click.group()
@click.pass_context
@click.option(
    "--profile",
    "-p",
    is_flag=False,
    help="set the profile with which to run the ATC CLI",
)
def atc(ctx, profile):
    """
    Main command for the CLI
    """
    _atc = ATC(profile=profile)
    _atc.load_config()
    ctx.obj = _atc


@atc.group()
@click.pass_obj
def create(_atc: ATC):
    """
    sub-command to perform create operations on supported resources
    """


@atc.group()
@click.pass_obj
def delete(_atc: ATC):
    """
    sub-command to perform delete operations on supported resources
    """


@atc.group()
@click.pass_obj
def get(_atc: ATC):
    """
    sub-command to perform get operations on supported resources
    """


@get.command("status")
@click.pass_obj
def __get_status(_atc: ATC):
    get_status(_atc)


if __name__ == "__main__":
    # ignore no value for argument `ctx` `profile` these are populated by click's `option()` decorator when called
    # pylint: disable=E1120
    atc()
