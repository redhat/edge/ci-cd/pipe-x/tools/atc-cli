"""
status.commands.py is the module containing all commands for status type function calls, I.E this module is where
any functionality relating to the retrieval of statuses, or, at least  ones which do not fit into library-specific
status calls, should be implemented
"""

import requests
from tabulate import tabulate

from utils.config import ATC


def get_status(atc: ATC):
    """
    INFO: Example function for now, will need more work after PoC
    TODO:  add more status calls here for each service
        - Use Logger
    :return:
    """
    table = [["endpoint", "url", "status", "reason"]]

    if not atc.requirements_configured(["webserver"]):
        # TODO: improve the error print out
        raise ValueError("required configuration fields for retrieving status have not been set")

    response = get_http_endpoint_health(atc.config["webserver"])
    table.append(["webserver", atc.config["webserver"], response, ""])

    print(tabulate(table, headers="firstrow", tablefmt="presto"))


def get_http_endpoint_health(url: str) -> str:
    """
    :param url: the http endpoint for which the health check should be performed
    :return: String representation of http status_code
    """
    try:
        response = requests.get(url, timeout=30)
        return f"{response.status_code}"
    except requests.exceptions.RequestException as e:
        return f"{e}"
