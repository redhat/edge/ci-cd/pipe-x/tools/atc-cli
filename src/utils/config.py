import os
from datetime import datetime, timedelta
from tomllib import load, loads

import requests
from requests import RequestException
from tomli_w import dump as dump_toml

ATC_USER_CONFIG_ENV_VAR = "ATC_USER_CONFIG"

HOME_ENV = os.getenv("HOME")
ATC_DEFAULT_CONFIG_DIR = f"{HOME_ENV}/.config/atc"

ATC_DEFAULT_CACHE_CONFIG_PATH = f"{HOME_ENV}/.cache/atc/atc.toml"
ATC_DEFAULT_USER_CONFIG_PATH = f"{ATC_DEFAULT_CONFIG_DIR}/user_conf.toml"


class ATC:
    """
    ATC represents the configuration object
    """

    def __init__(
        self,
        user_config_filepath: str = ATC_DEFAULT_USER_CONFIG_PATH,
        profile: str = "autosd",
    ):
        self.config = {}
        self.user_config_filepath = user_config_filepath
        self.profile = profile

    def load_config(self):
        """
        Loads the profile configuration into memory
        """
        user_config = _read_config(self.user_config_filepath, ATC_USER_CONFIG_ENV_VAR)

        if not user_config:
            # TODO make this error message more helpful, link to eventual docs
            raise ValueError("User configuration returned empty, please populate it with config")

        # Read the user config, create an empty cache if it doesn't exist
        cache_config = _read_config(ATC_DEFAULT_CACHE_CONFIG_PATH, create_if_not_exists=True)

        # Iterate over all profiles configured in the user config, regardless of if they are used or not
        for profile in user_config.keys():
            try:
                # Validate that all user profiles have a remote_config_url set, fail otherwise
                _ = user_config[profile]["remote_config_url"]
            except KeyError:
                raise ValueError(f"remote_config_url must be set for {profile}")

        # Check whether the cache config is empty, or is out of date by a day, update if so
        sync_needed = (
            cache_config is None
            or len(cache_config) == 0
            or (cache_config.get("metadata", {}).get("last_update", datetime.min) + timedelta(days=1) < datetime.now())
        )

        if sync_needed:
            # Synchronise all remote configs to cache
            _sync_config_cache_from_remote([section["remote_config_url"] for section in user_config.values()])
            # Once synchronised, read cache_config
            cache_config = _read_config(ATC_DEFAULT_CACHE_CONFIG_PATH)

        # Merge user config and remote config (from cache) into one dictionary, set as config for current run
        self.config = {**cache_config[self.profile], **user_config[self.profile]}

    def requirements_configured(self, reqs: list[str]) -> bool:
        """
        Validate the list of required variables is configured in the config
        :param reqs: Declares required non-empty variables within the `field`
        :return: boolean indicating whether `reqs` fields are present in the config
        """

        for req in reqs:
            if self.config.get(req) is None:
                return False
        return True


def _sync_config_cache_from_remote(urls: list[str]):
    """
    :param urls: list of str defining the remote configs to be written into cache config
    :return:
    """
    remote_config = {}
    for url in urls:
        fetched_conf = _fetch_remote_config(url)
        remote_config = {**remote_config, **fetched_conf}

    # inject metadata
    remote_config = {**remote_config, **{"metadata": {"last_update": datetime.now()}}}

    _write_config(ATC_DEFAULT_CACHE_CONFIG_PATH, remote_config)


def _fetch_remote_config(url) -> dict:
    """
    :param url: The address of the remote `.toml` based config
    :return: dict with remote configuration from `url`
    """
    try:
        response = requests.get(url, timeout=30)
        response.raise_for_status()
        return loads(response.text)
    except RequestException as e:
        print(f"an error occurred fetching configuration from {url}, error: {e}")
    return {}


def _read_config(filepath: str, env_var: str = "", create_if_not_exists: bool = False) -> dict:
    """
    Reads configuration from a specified filepath, or env_var-based filepath
    :param filepath: declares the filepath of a `.toml` file to be read into the dictionary
    :param env_var: declares an override env_var for `filepath`, env_var will take precedence
    :param create_if_not_exists: create the file if it doesn't exist, mainly used for cache pre-creation
    :return: dict representation of `filepath` `.toml` content
    """
    env_config_path = None
    if env_var:
        env_config_path = os.getenv(env_var)
    filepath = env_config_path if env_config_path is not None else filepath

    if filepath is None:
        raise ValueError(f"both filepath and {env_var} are none, can't do anything.")

    config = {}

    try:
        with open(filepath, "rb") as f:
            config = load(f)
            if config is not None:
                return config
    except FileNotFoundError as e:
        print(f"could not find or parse the configuration file at path {filepath}. Error: {e}")
        if create_if_not_exists:
            print(f"attempting to create an empty file at: {filepath}")
            _write_config(filepath, config)

    return config


def _write_config(filepath: str, conf: dict):
    """
    writes the specified `conf` dictionary in .toml format to the specified `filepath`
    :param filepath: declares the filepath to which the `.toml` based dictionary should be written
    :param conf: declares the dictionary to be written to `filepath`
    :return:
    """
    try:
        os.makedirs(os.path.dirname(filepath), exist_ok=True)
        with open(filepath, "wb+") as f:
            dump_toml(conf, f)
    except OSError as e:
        print(f"an error occurred trying to write to {filepath}, error: {e}")
